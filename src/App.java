import model.Date;

public class App {
    public static void main(String[] args) throws Exception {
        Date date1 = new Date();
        date1.setDay(5);
        date1.setMonth(2);
        date1.setYear(2000);
        System.out.println(date1.toString());
        Date date2 = new Date(18, 12, 2000);
        System.out.println(date2.toString());
    }
}
