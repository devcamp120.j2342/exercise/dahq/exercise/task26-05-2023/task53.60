package model;

import org.w3c.dom.events.MouseEvent;

public class Date {
    private int day;
    private int month;
    private int year;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if (day > 0 && day < 32) {
            this.day = day;
        } else {
            System.out.println("Nhap sai ngay");
        }

    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (month > 0 && month < 13) {
            this.month = month;
        } else {
            System.out.println("Nhap sai thang");
        }

    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if (year >= 1900 && year <= 9999) {
            this.year = year;
        } else {
            System.out.println("Nhap sai nam");
        }

    }

    public Date() {
    }

    public Date(int day, int month, int year) {
        if (day > 0 && day < 32) {
            this.day = day;
        } else {
            System.out.println("Nhap sai ngay");
        }

        if (month > 0 && month < 13) {
            this.month = month;
        } else {
            System.out.println("nhap sai thang");
        }

        if (year >= 1990 && year <= 9990) {
            this.year = year;
        } else {
            System.out.println("Nhap sai nam");
        }

    }

    @Override
    public String toString() {
        if (this.day < 10 && this.month < 10) {
            return "0" + this.day + "/" + "0" + this.month + "/" + this.year;
        }
        if (this.month < 10) {
            return this.day + "/" + "0" + this.month + "/" + this.year;
        }
        if (this.day < 10) {
            return "0" + this.day + "/" + this.month + "/" + this.year;
        }
        return this.day + "/" + this.month + "/" + this.year;
    }

}
